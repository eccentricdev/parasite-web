import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestEquipmentListComponent } from './request-equipment-list.component';

describe('RequestEquipmentListComponent', () => {
  let component: RequestEquipmentListComponent;
  let fixture: ComponentFixture<RequestEquipmentListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestEquipmentListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestEquipmentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
