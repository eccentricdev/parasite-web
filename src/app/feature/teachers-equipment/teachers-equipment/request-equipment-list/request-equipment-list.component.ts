import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2'

@Component({
  selector: 'app-request-equipment-list',
  templateUrl: './request-equipment-list.component.html',
  styleUrls: ['./request-equipment-list.component.scss']
})
export class RequestEquipmentListComponent implements OnInit {
  rows:any = [{n:1}]
  constructor(
    public router : Router
  ) { }

  ngOnInit(): void {
  }
  addPage(){
    this.router.navigate(['app/teachers-equipment/add'])
  }
  editPage(id){
    this.router.navigate(['app/teachers-equipment/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        


        }
      })
    }
  }
}
