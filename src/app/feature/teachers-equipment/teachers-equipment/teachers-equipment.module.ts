import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { RequestEquipmentListComponent } from './request-equipment-list/request-equipment-list.component';
import { RequestEquipmentFormComponent } from './request-equipment-form/request-equipment-form.component';

const routes:Routes = [
  {
    path:'list',
    component:RequestEquipmentListComponent
  },
  {
    path:'add',
    component:RequestEquipmentFormComponent
  },
  
  
]

@NgModule({
  declarations: [RequestEquipmentListComponent, RequestEquipmentFormComponent,],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class TeachersEquipmentModule { }
