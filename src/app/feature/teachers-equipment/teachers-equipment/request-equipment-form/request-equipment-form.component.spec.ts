import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestEquipmentFormComponent } from './request-equipment-form.component';

describe('RequestEquipmentFormComponent', () => {
  let component: RequestEquipmentFormComponent;
  let fixture: ComponentFixture<RequestEquipmentFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestEquipmentFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestEquipmentFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
