import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-request-equipment-form',
  templateUrl: './request-equipment-form.component.html',
  styleUrls: ['./request-equipment-form.component.scss']
})
export class RequestEquipmentFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  itemArray =[{n:1}]
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public dialog: MatDialog,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
  close(){
    this.router.navigate(['app/teachers-equipment/list'])
  }
  save() {
    this.router.navigate(['app/teachers-equipment/list'])

  }
  // openPopUp() {
  //   const dialogRef = this.dialog.open(
  //     RequestItemDialogComponent, {
  //     disableClose: true,
  //     width: '70%',
  //     data: {  } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
  //   }
  //   )

  //   dialogRef.afterClosed().subscribe(callback => {
  //     console.log(callback)
  //   })

  // }
  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }
}
