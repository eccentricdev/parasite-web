import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PurchaseRequestItemDialogComponent } from './purchase-request-item-dialog.component';

describe('PurchaseRequestItemDialogComponent', () => {
  let component: PurchaseRequestItemDialogComponent;
  let fixture: ComponentFixture<PurchaseRequestItemDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PurchaseRequestItemDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PurchaseRequestItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
