import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { PurchaseRequestItemDialogComponent } from '../purchase-request-item-dialog/purchase-request-item-dialog.component';

@Component({
  selector: 'app-purchase-request-form',
  templateUrl: './purchase-request-form.component.html',
  styleUrls: ['./purchase-request-form.component.scss']
})
export class PurchaseRequestFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  itemArray =[{n:1}]
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public dialog: MatDialog,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
  close(){
    this.router.navigate(['app/purchase-request'])
  }
  save() {
    this.router.navigate(['app/purchase-request'])

  }
  openPopUp() {
    const dialogRef = this.dialog.open(
      PurchaseRequestItemDialogComponent, {
      disableClose: true,
      width: '70%',
      data: {  } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
    })

  }
  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }
}
