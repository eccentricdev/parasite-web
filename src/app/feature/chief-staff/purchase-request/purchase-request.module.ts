import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchaseRequestListComponent } from './purchase-request-list/purchase-request-list.component';
import { PurchaseRequestFormComponent } from './purchase-request-form/purchase-request-form.component';
import { PurchaseRequestItemDialogComponent } from './purchase-request-item-dialog/purchase-request-item-dialog.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:PurchaseRequestListComponent
  },
  {
    path:'add',
    component:PurchaseRequestFormComponent
  },
  {
    path:'edit/:id',
    component:PurchaseRequestFormComponent
  },
  
]

@NgModule({
  declarations: [PurchaseRequestListComponent, PurchaseRequestFormComponent, PurchaseRequestItemDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class PurchaseRequestModule { }
