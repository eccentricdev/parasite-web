import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-propose-requirements-budget-dialog',
  templateUrl: './propose-requirements-budget-dialog.component.html',
  styleUrls: ['./propose-requirements-budget-dialog.component.scss']
})
export class ProposeRequirementsBudgetDialogComponent implements OnInit {

  constructor( public dialogRef: MatDialogRef<ProposeRequirementsBudgetDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,) { }

  ngOnInit(): void {
  }
  closeDialog() {
    this.dialogRef.close('close')
  }
}
