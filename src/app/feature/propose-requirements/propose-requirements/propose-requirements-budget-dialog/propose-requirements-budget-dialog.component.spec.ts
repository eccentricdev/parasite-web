import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposeRequirementsBudgetDialogComponent } from './propose-requirements-budget-dialog.component';

describe('ProposeRequirementsBudgetDialogComponent', () => {
  let component: ProposeRequirementsBudgetDialogComponent;
  let fixture: ComponentFixture<ProposeRequirementsBudgetDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposeRequirementsBudgetDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposeRequirementsBudgetDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
