import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposeRequirementsFormComponent } from './propose-requirements-form.component';

describe('ProposeRequirementsFormComponent', () => {
  let component: ProposeRequirementsFormComponent;
  let fixture: ComponentFixture<ProposeRequirementsFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposeRequirementsFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposeRequirementsFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
