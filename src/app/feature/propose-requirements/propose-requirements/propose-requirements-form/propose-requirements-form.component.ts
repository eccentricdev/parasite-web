import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { ProposeRequirementsBudgetDialogComponent } from '../propose-requirements-budget-dialog/propose-requirements-budget-dialog.component';
import { ProposeRequirementsItemDialogComponent } from '../propose-requirements-item-dialog/propose-requirements-item-dialog.component';

@Component({
  selector: 'app-propose-requirements-form',
  templateUrl: './propose-requirements-form.component.html',
  styleUrls: ['./propose-requirements-form.component.scss']
})
export class ProposeRequirementsFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  itemArray =[{n:1}]
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public dialog: MatDialog,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
  close(){
    this.router.navigate(['app/propose-requirements'])
  }
  save() {
    this.router.navigate(['app/propose-requirements'])

  }
  openPopUp() {
    const dialogRef = this.dialog.open(
      ProposeRequirementsItemDialogComponent, {
      disableClose: true,
      width: '70%',
      data: {  } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
    })

  }
  openPopUp2() {
    const dialogRef = this.dialog.open(
      ProposeRequirementsBudgetDialogComponent, {
      disableClose: true,
      width: '70%',
      data: {  } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
    })

  }
  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }
}
