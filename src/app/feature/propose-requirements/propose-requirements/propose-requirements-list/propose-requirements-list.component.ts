import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2'

@Component({
  selector: 'app-propose-requirements-list',
  templateUrl: './propose-requirements-list.component.html',
  styleUrls: ['./propose-requirements-list.component.scss']
})
export class ProposeRequirementsListComponent implements OnInit {
  rows:any = [{n:1}]
  constructor(
    public router : Router
  ) { }

  ngOnInit(): void {
  }
  addPage(){
    this.router.navigate(['app/propose-requirements/add'])
  }
  editPage(id){
    this.router.navigate(['app/propose-requirements/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        


        }
      })
    }
  }
}
