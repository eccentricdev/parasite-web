import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposeRequirementsListComponent } from './propose-requirements-list.component';

describe('ProposeRequirementsListComponent', () => {
  let component: ProposeRequirementsListComponent;
  let fixture: ComponentFixture<ProposeRequirementsListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposeRequirementsListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposeRequirementsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
