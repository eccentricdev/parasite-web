import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProposeRequirementsItemDialogComponent } from './propose-requirements-item-dialog.component';

describe('ProposeRequirementsItemDialogComponent', () => {
  let component: ProposeRequirementsItemDialogComponent;
  let fixture: ComponentFixture<ProposeRequirementsItemDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProposeRequirementsItemDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProposeRequirementsItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
