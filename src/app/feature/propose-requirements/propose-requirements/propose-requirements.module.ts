import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProposeRequirementsFormComponent } from './propose-requirements-form/propose-requirements-form.component';
import { ProposeRequirementsListComponent } from './propose-requirements-list/propose-requirements-list.component';
import { ProposeRequirementsItemDialogComponent } from './propose-requirements-item-dialog/propose-requirements-item-dialog.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { ProposeRequirementsBudgetDialogComponent } from './propose-requirements-budget-dialog/propose-requirements-budget-dialog.component';

const routes:Routes = [
  {
    path:'',
    component:ProposeRequirementsListComponent
  },
  {
    path:'add',
    component:ProposeRequirementsFormComponent
  },
  {
    path:'edit/:id',
    component:ProposeRequirementsFormComponent
  },
  
]


@NgModule({
  declarations: [ProposeRequirementsListComponent,ProposeRequirementsFormComponent,ProposeRequirementsItemDialogComponent, ProposeRequirementsBudgetDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ProposeRequirementsModule { }
