import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';
import { RequestItemDialogComponent } from '../request-item-dialog/request-item-dialog.component';

@Component({
  selector: 'app-request-form-view',
  templateUrl: './request-form-view.component.html',
  styleUrls: ['./request-form-view.component.scss']
})
export class RequestFormViewComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  itemArray =[{n:1}]
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
    public dialog: MatDialog,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
  close(){
    this.router.navigate(['app/propose-requirements'])
  }
  save() {
    this.router.navigate(['app/propose-requirements'])

  }
  openPopUp() {
    const dialogRef = this.dialog.open(
      RequestItemDialogComponent, {
      disableClose: true,
      width: '70%',
      data: {  } // ใส่ข้อมูลที่จะส่งไปหน้า dialog นะ          
    }
    )

    dialogRef.afterClosed().subscribe(callback => {
      console.log(callback)
    })

  }
  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }
}
