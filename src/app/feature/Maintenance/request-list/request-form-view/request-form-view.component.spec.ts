import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestFormViewComponent } from './request-form-view.component';

describe('RequestFormViewComponent', () => {
  let component: RequestFormViewComponent;
  let fixture: ComponentFixture<RequestFormViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RequestFormViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestFormViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
