import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RequestListComponent } from './request-list/request-list.component';
import { RequestFormComponent } from './request-form/request-form.component';
import { RequestItemDialogComponent } from './request-item-dialog/request-item-dialog.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';
import { RequestFormViewComponent } from './request-form-view/request-form-view.component';

const routes:Routes = [
  {
    path:'',
    component:RequestListComponent
  },
  {
    path:'add',
    component:RequestFormComponent
  },
  {
    path:'edit/:id',
    component:RequestFormComponent
  },
  {
    path:'view',
    component:RequestFormViewComponent
  },
  
]

@NgModule({
  declarations: [RequestListComponent, RequestFormComponent, RequestItemDialogComponent, RequestFormViewComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class RequestListModule { }
