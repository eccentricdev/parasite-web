import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppovePlanListComponent } from './appove-plan-list/appove-plan-list.component';
import { AppovePlanFormComponent } from './appove-plan-form/appove-plan-form.component';
import { AppovePlanItemDialogComponent } from './appove-plan-item-dialog/appove-plan-item-dialog.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';


const routes:Routes = [
  {
    path:'',
    component:AppovePlanListComponent
  },
  {
    path:'add',
    component:AppovePlanFormComponent
  },
  {
    path:'edit/:id',
    component:AppovePlanFormComponent
  },
  
]

@NgModule({
  declarations: [AppovePlanListComponent, AppovePlanFormComponent, AppovePlanItemDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ApprovePlanModule { }
