import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppovePlanItemDialogComponent } from './appove-plan-item-dialog.component';

describe('AppovePlanItemDialogComponent', () => {
  let component: AppovePlanItemDialogComponent;
  let fixture: ComponentFixture<AppovePlanItemDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppovePlanItemDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppovePlanItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
