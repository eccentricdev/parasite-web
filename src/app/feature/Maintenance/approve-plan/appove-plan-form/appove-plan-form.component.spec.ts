import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppovePlanFormComponent } from './appove-plan-form.component';

describe('AppovePlanFormComponent', () => {
  let component: AppovePlanFormComponent;
  let fixture: ComponentFixture<AppovePlanFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppovePlanFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppovePlanFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
