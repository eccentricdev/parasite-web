import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2'

@Component({
  selector: 'app-appove-plan-list',
  templateUrl: './appove-plan-list.component.html',
  styleUrls: ['./appove-plan-list.component.scss']
})
export class AppovePlanListComponent implements OnInit {
  rows:any = [{n:1}]
  constructor(
    public router : Router
  ) { }

  ngOnInit(): void {
  }
  addPage(){
    this.router.navigate(['app/approve-plan/add'])
  }
  editPage(id){
    this.router.navigate(['app/approve-plan/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        


        }
      })
    }
  }
}
