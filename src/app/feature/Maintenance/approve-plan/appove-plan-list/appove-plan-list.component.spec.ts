import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppovePlanListComponent } from './appove-plan-list.component';

describe('AppovePlanListComponent', () => {
  let component: AppovePlanListComponent;
  let fixture: ComponentFixture<AppovePlanListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AppovePlanListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AppovePlanListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
