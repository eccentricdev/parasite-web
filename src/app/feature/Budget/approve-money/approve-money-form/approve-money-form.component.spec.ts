import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveMoneyFormComponent } from './approve-money-form.component';

describe('ApproveMoneyFormComponent', () => {
  let component: ApproveMoneyFormComponent;
  let fixture: ComponentFixture<ApproveMoneyFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApproveMoneyFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveMoneyFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
