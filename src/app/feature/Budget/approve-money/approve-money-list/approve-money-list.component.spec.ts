import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveMoneyListComponent } from './approve-money-list.component';

describe('ApproveMoneyListComponent', () => {
  let component: ApproveMoneyListComponent;
  let fixture: ComponentFixture<ApproveMoneyListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApproveMoneyListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveMoneyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
