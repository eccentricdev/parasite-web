import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import swal from 'sweetalert2'

@Component({
  selector: 'app-approve-money-list',
  templateUrl: './approve-money-list.component.html',
  styleUrls: ['./approve-money-list.component.scss']
})
export class ApproveMoneyListComponent implements OnInit {
  rows:any = [{n:1}]
  constructor(
    public router : Router
  ) { }

  ngOnInit(): void {
  }
  addPage(){
    this.router.navigate(['app/approve-money/add'])
  }
  editPage(id){
    this.router.navigate(['app/approve-money/edit',id])
  }

  deleteItem(id){
    {
      swal.getTitle()
      swal.fire({
        text: 'ยืนยันการลบรายการ',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'ตกลง',
        cancelButtonText: 'ยกเลิก',
      }).then((result) => {
        console.log(result)

        if (result.value) {
        


        }
      })
    }
  }
}
