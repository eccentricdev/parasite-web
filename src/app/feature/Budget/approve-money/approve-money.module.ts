import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApproveMoneyListComponent } from './approve-money-list/approve-money-list.component';
import { ApproveMoneyFormComponent } from './approve-money-form/approve-money-form.component';
import { ApproveMoneyItemDialogComponent } from './approve-money-item-dialog/approve-money-item-dialog.component';
import { Routes, RouterModule } from '@angular/router';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:ApproveMoneyListComponent
  },
  {
    path:'add',
    component:ApproveMoneyFormComponent
  },
  {
    path:'edit/:id',
    component:ApproveMoneyFormComponent
  },
  
]

@NgModule({
  declarations: [ApproveMoneyListComponent, ApproveMoneyFormComponent, ApproveMoneyItemDialogComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class ApproveMoneyModule { }
