import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-approve-money-item-dialog',
  templateUrl: './approve-money-item-dialog.component.html',
  styleUrls: ['./approve-money-item-dialog.component.scss']
})
export class ApproveMoneyItemDialogComponent implements OnInit {
  itemData = []
  procurementGroupData = [
    {
      name : 'น้ำยาและเคมีสาร',
      id : 1
    },
    {
      name : 'วัสดุสิ้นเปลือง',
      id:2
    },
  ]
  constructor(
    public dialogRef: MatDialogRef<ApproveMoneyItemDialogComponent>,
    @Inject(MAT_DIALOG_DATA)
    public data: any,
  ) { }

  ngOnInit(): void {
  }

  change(data){
    if(data == 1){
      this.itemData = [
        {
          name : 'ชุดทดสอบความไวตอยาระดับปกติของเชื้อ Mycobacterium tuberculosis สําหรับใชกับเครื่องเพาะเชื้อวัณโรคอัตโนมัติ (Antimycobacterium susceptibility testing kit)',
          id : 1,
          procurementGroupId:1
        },
        {
          name : 'ชุดน้ำยา Growth supplement  และ Antibiotic mixture สําหรับอาหารเลี้ยงเชื้อ Mycobacterium tuberculosis  (Growth supplement and antibiotic mixture for Mycobacterium tuberculosis culture media) บรรจุ 100 เทสท์',
          id : 2,
          procurementGroupId:1
        },
        {
          name : 'อาหารเลี้ยงเชื้อไมโคแบคทีเรียมแบบบรรจุในหลอด (Mycobacterial Growth Indicator Tube) 7 มล.',
          id : 3,
          procurementGroupId:1
        },
        {
          name : 'อาหารเลี้ยงเชื้อไมโคแบคทีเรียมสําหรับใชกับเครื่องตรวจหาเชื้อในเลือดแบบอัตโนมัติ  (Mycobacterial Medium) 40 มล.',
          id : 4,
          procurementGroupId:1
        },
        {
          name : 'น้ำยาทดสอบประสิทธิภาพการทําใหปราศจากเชื้อของเครื่องนึ่งฆาเชื้อดวยไอน้ำโดยวิธีชีวภาพ   ( Biological indicator for steam sterilization) 100 หลอด/ชุด',
          id : 5,
          procurementGroupId:1
        },
      ]
    }else{
      this.itemData = [
        {
          name : 'ไปเปต ทิพ (Pipette Tip) ขนาดต่างๆ (ขนาด 10-200 ไมโครลิตร ถุงละ 1000 อัน สีเหลือง)',
          id : 6,
          procurementGroupId:2
        },
        {
          name : 'ไปเปต ทิพ (Pipette Tip) ขนาดต่างๆ (ขนาด 0.5-10 ไมโครลิตร ถุงละ 1000 อัน)',
          id : 7,
          procurementGroupId:2
        },
        {
          name : 'ไปเปต ทิพ (Pipette Tip) ขนาดต่างๆ (ขนาด 200-1000 ไมโครลิตร ถุงละ 1000 อัน สีฟ้า)',
          id : 8,
          procurementGroupId:2
        },
        {
          name : 'ไปเปตทิปพลาสติกขนาดต่าง ๆ ชนิดมีแผ่นกรอง ( Pipette tip with filter) ขนาด  200 ไมโครลิตร',
          id : 9,
          procurementGroupId:2
        },
      ]
    }
  }
  closeDialog() {
    this.dialogRef.close('close')
  }
  
}
