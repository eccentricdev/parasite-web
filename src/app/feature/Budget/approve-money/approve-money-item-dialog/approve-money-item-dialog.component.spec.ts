import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApproveMoneyItemDialogComponent } from './approve-money-item-dialog.component';

describe('ApproveMoneyItemDialogComponent', () => {
  let component: ApproveMoneyItemDialogComponent;
  let fixture: ComponentFixture<ApproveMoneyItemDialogComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApproveMoneyItemDialogComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApproveMoneyItemDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
