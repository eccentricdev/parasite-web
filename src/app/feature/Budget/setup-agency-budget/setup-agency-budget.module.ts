import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SetupAgencyBudgetListComponent } from './setup-agency-budget-list/setup-agency-budget-list.component';
import { SetupAgencyBudgetFormComponent } from './setup-agency-budget-form/setup-agency-budget-form.component';
import { SharedModule } from 'src/app/shared/shared.module';

const routes:Routes = [
  {
    path:'',
    component:SetupAgencyBudgetListComponent
  },
  {
    path:'add',
    component:SetupAgencyBudgetFormComponent
  },
  {
    path:'edit/:id',
    component:SetupAgencyBudgetFormComponent
  },
  
]

@NgModule({
  declarations: [SetupAgencyBudgetListComponent,SetupAgencyBudgetFormComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    SharedModule
  ]
})
export class SetupAgencyBudgetModule { }
