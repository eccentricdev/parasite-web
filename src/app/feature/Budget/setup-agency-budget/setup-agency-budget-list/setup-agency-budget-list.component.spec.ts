import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupAgencyBudgetListComponent } from './setup-agency-budget-list.component';

describe('SetupAgencyBudgetListComponent', () => {
  let component: SetupAgencyBudgetListComponent;
  let fixture: ComponentFixture<SetupAgencyBudgetListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupAgencyBudgetListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupAgencyBudgetListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
