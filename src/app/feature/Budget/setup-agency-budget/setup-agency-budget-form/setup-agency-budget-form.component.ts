import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseForm } from 'src/app/core/base/base-form';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-setup-agency-budget-form',
  templateUrl: './setup-agency-budget-form.component.html',
  styleUrls: ['./setup-agency-budget-form.component.scss']
})
export class SetupAgencyBudgetFormComponent extends BaseForm implements OnInit {
  academicYear = []
  semesterData = []
  constructor(public FormBuilder: FormBuilder,
    public router:Router,
    public activeRoute: ActivatedRoute,
    public appSV: AppService,
  ) { super(FormBuilder, activeRoute) 
   
  }

  ngOnInit(): void {
    console.log(this.id)
    console.log(this.state)
    switch (this.state) {
      case 'edit':

        break;
      case 'add':

        break;
    }
  }

  dateChange() {
    // dateChange(form, date: moment.Moment, text) {
      // form.get(`${text}`).setValue(moment(date).add(7, 'hours').toDate())
    }
  close(){
    this.router.navigate(['app/setup-agency-budget'])
  }
  save() {
    this.router.navigate(['app/setup-agency-budget'])

  }
  createForm() {
    return this.baseFormBuilder.group({
     
    })
  }
}
