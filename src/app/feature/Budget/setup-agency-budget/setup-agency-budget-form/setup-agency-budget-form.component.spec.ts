import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SetupAgencyBudgetFormComponent } from './setup-agency-budget-form.component';

describe('SetupAgencyBudgetFormComponent', () => {
  let component: SetupAgencyBudgetFormComponent;
  let fixture: ComponentFixture<SetupAgencyBudgetFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SetupAgencyBudgetFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SetupAgencyBudgetFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
