import { OverlayContainer } from '@angular/cdk/overlay';
import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, timer } from 'rxjs';

@Component({
  selector: 'app-toolbar-layout',
  templateUrl: './toolbar-layout.component.html',
  styleUrls: ['./toolbar-layout.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolbarLayoutComponent implements OnInit {
  defaultFontSize = 16
  themeClass: string;
  isDarkTheme: boolean = true
  activeToolBar = 0

  constructor(
    public overlayContainer: OverlayContainer,
    public Router: Router,
    private cdRef: ChangeDetectorRef
  ) {    
  }

  ngOnInit(): void {
    // this.toogleTheme()
    timer(800, 800).pipe(
      // takeUntil(this.subject),
    ).subscribe(t => console.log(this.Router.url));
  }

  toogleTheme(){
      
      let newThemeClass = this.isDarkTheme ? 'dark-theme' : 'default-theme'
      this.isDarkTheme = !this.isDarkTheme
      
      const overlayContainerClasses = this.overlayContainer.getContainerElement().classList;
      const themeClassesToRemove = Array.from(overlayContainerClasses).filter((item: string) => item.includes('-theme'));
      if (themeClassesToRemove.length) {
         overlayContainerClasses.remove(...themeClassesToRemove);
         document.body.classList.remove(...themeClassesToRemove)
      }
      overlayContainerClasses.add(newThemeClass);
      document.body.classList.add(newThemeClass)
    }
  


  logout(){
    // window.location.href = 'https://rsu-iam.71dev.com/Account/Logout'
    this.Router.navigate(['/app/login'])
  }

  IncreaseFontSize(event: Event){    
    this.defaultFontSize ++
    document.body.style.fontSize = this.defaultFontSize + 'px';
    event.stopPropagation()
  }

  DecreaseFontSize(event: Event){
    this.defaultFontSize --
    document.body.style.fontSize = this.defaultFontSize + 'px';
    event.stopPropagation()
  }
}
