import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-home-menu',
  templateUrl: './home-menu.component.html',
  styleUrls: ['./home-menu.component.scss']
})
export class HomeMenuComponent implements OnInit {
  user : any
  pass : any
  constructor(public router :Router,
              public AppService:AppService) { }

  ngOnInit(): void {
    timer(800, 800).pipe(
      ).subscribe(t => {
        this.AppService.user$.pipe(
          tap(x=>this.user = x),
          tap(x=>console.log(x)),
        ).subscribe()
      });
  }
  onClickMenu(){
    console.log('click',this.user)
    if(this.user == 'user1'){
      this.router.navigate(['/app/setup-agency-budget'])
    }else if(this.user == 'user2'){
      this.router.navigate(['/app/propose-requirements'])
    }else if(this.user == 'user3'){
      this.router.navigate(['/app/request'])
    }else{
      this.router.navigate(['/app/purchase-request'])
    }
      // this.router.navigate(['/app'])
    // if(this.user == 'user1'){
    //   this.router.navigate(['/app/menu'])
    // }else if(this.user == 'user2'){
    //   this.router.navigate(['/app/menu2'])
    // }
    

    
  }
}
