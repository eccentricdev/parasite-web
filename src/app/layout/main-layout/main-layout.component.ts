import { Component, OnInit, ChangeDetectionStrategy, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { timer } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AppService } from 'src/app/core/service/app.service';

@Component({
  selector: 'app-main-layout',
  templateUrl: './main-layout.component.html',
  styleUrls: ['./main-layout.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainLayoutComponent implements OnInit {
  isExpandMenu: boolean = true
  user : any
  constructor(public router:Router,
              public AppService:AppService
              ) { }

  ngOnInit(): void {

    timer(800, 800).pipe(
    ).subscribe(t => {
      this.AppService.user$.pipe(
        tap(x=>this.user = x),
        tap(x=>console.log(x)),
      ).subscribe()
    });





   
  }

}
