import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-menu-user2',
  templateUrl: './menu-user2.component.html',
  styleUrls: ['./menu-user2.component.scss']
})
export class MenuUser2Component implements OnInit {

  @Output() onToggleMenu = new EventEmitter<boolean>()
  isExpandMenu: boolean = true
  
  constructor(
    // public odic: OidsAuthService,
    
  ) { }

  ngOnInit(): void {
    // console.log(this.odic.user)
    }
  windowsSlid(){
    window.scrollTo({ top: 0, behavior: 'smooth' })
  }
  toggleMenu(){
    this.isExpandMenu = !this.isExpandMenu
    this.onToggleMenu.emit(this.isExpandMenu)
  }
}
