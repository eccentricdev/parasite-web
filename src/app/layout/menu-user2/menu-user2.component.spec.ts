import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuUser2Component } from './menu-user2.component';

describe('MenuUser2Component', () => {
  let component: MenuUser2Component;
  let fixture: ComponentFixture<MenuUser2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MenuUser2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MenuUser2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
