import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from 'src/app/core/service/app.service';


@Component({
  selector: 'log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss'],
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogInComponent implements OnInit {
  user : any 
  password : any
  constructor(
    public router:Router,
    public AppService:AppService

  ) { }

  ngOnInit(): void {
   
  }


  login(){
    console.log(this.user)
    if(this.user != 'user1' && this.user != 'user2' && this.user != 'user3' && this.user != 'user4'){
      this.AppService.swaltAlertError('','ชื่อผู้ใช้งานไม่ถูกต้อง')
    }else{
      this.router.navigate(['/app/home-menu'])
      this.AppService.user$.next(this.user)
      this.AppService.pass$.next(this.password)
    }
  }
  // openPopUp(){
  //   const dialogRef = this.dialog.open(DialogDemoComponent, {
  //     width: '250px',
  //     data: {data:'demoDialog'}
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     console.log('The dialog was closed');
      
  //   });
  // }

}
