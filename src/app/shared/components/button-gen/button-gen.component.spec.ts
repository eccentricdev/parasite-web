import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonGenComponent } from './button-gen.component';

describe('ButtonGenComponent', () => {
  let component: ButtonGenComponent;
  let fixture: ComponentFixture<ButtonGenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonGenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonGenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
