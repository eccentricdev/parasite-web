import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonApprovalComponent } from './button-approval.component';

describe('ButtonApprovalComponent', () => {
  let component: ButtonApprovalComponent;
  let fixture: ComponentFixture<ButtonApprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonApprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonApprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
