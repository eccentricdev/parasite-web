import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ButtonDisapprovalComponent } from './button-disapproval.component';

describe('ButtonDisapprovalComponent', () => {
  let component: ButtonDisapprovalComponent;
  let fixture: ComponentFixture<ButtonDisapprovalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ButtonDisapprovalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ButtonDisapprovalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
