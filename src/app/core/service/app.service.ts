import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import swal from 'sweetalert2'

@Injectable({
  providedIn: 'root'
})
export class AppService {
  public user$  = new BehaviorSubject(null)
  public pass$  = new BehaviorSubject(null)
  constructor() { }
  swaltAlert(title: string = 'บันทึก', text: string = 'บันทึกสำเร็จ') {
    swal.fire({
      title: title,
      text: text,
      icon: 'success',
      // confirmButtonText: 'Cool'
    })
  }
  swaltAlertInfo(title: string = '', text: string = '') {
    swal.fire({
      title: title,
      text: text,
      icon: 'info'
      // confirmButtonText: 'Cool'
    })
  }

  swaltAlertError(title: string = 'ผิดพลาด', text: string) {
    swal.fire({
      title: title,
      text: text,
      icon: 'error',
      // confirmButtonText: 'Cool'
    })
  }


}
