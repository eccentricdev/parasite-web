import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { LogInComponent } from './layout/log-in/log-in.component';
import { HomeMenuComponent } from './layout/home-menu/home-menu.component';


const routes: Routes = [ 
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'app/login'
  }, 
    {
      path:'app/login',
      component:LogInComponent,
    },
    {
      path:'',
      component: MainLayoutComponent,
      // loadChildren: () => import('./feature/home-menu/home-menu.module')
      // .then(x => x.HomeMenuModule),
      pathMatch:'full'
    },
    {
      path: "app/home-menu",
      component:HomeMenuComponent,
    },
    // {
    //   path: "app/menu",
    //   component:MainLayoutComponent,
    // },
    // {
    //   path: "app/menu2",
    //   component:MainLayoutComponent,
    // },

    {

    path: 'app',
    component: MainLayoutComponent,
    children: [
      // {
      //   path: '',
      //   redirectTo: 'home'
      // },
      {
        path:'**',
        redirectTo:'app/login'
      },
      {
        path:'setup-agency-budget',
        loadChildren: () => import('./feature/Budget/setup-agency-budget/setup-agency-budget.module')
          .then(x => x.SetupAgencyBudgetModule)
      },
      {
        path:'approve-money',
        loadChildren: () => import('./feature/Budget/approve-money/approve-money.module')
          .then(x => x.ApproveMoneyModule)
      },
      {
        path:'request',
        loadChildren: () => import('./feature/Maintenance/request-list/request-list.module')
          .then(x => x.RequestListModule)
      },
      {
        path:'approve-plan',
        loadChildren: () => import('./feature/Maintenance/approve-plan/approve-plan.module')
          .then(x => x.ApprovePlanModule)
      },
      {
        path:'purchase-request',
        loadChildren: () => import('./feature/chief-staff/purchase-request/purchase-request.module')
          .then(x => x.PurchaseRequestModule)
      },


      {
        path:'propose-requirements',
        loadChildren: () => import('./feature/propose-requirements/propose-requirements/propose-requirements.module')
          .then(x => x.ProposeRequirementsModule)
      },
      {
        path:'teachers-equipment',
        loadChildren: () => import('./feature/teachers-equipment/teachers-equipment/teachers-equipment.module')
          .then(x => x.TeachersEquipmentModule)
      },
    ]
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
