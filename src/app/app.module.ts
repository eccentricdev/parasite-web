import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';
import { CoreModule } from './core/core.module';
import { ToolbarLayoutComponent } from './layout/toolbar-layout/toolbar-layout.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { LogInComponent } from './layout/log-in/log-in.component';
import { EditorModule } from '@tinymce/tinymce-angular';
import { SharedModule } from './shared/shared.module';
import { MenuComponent } from './layout/menu/menu.component';
import { LoadingComponent } from './layout/loading/loading.component';
import { HomeMenuComponent } from './layout/home-menu/home-menu.component';
import { MenuUser2Component } from './layout/menu-user2/menu-user2.component';






@NgModule({
  declarations: [
    AppComponent,
    MainLayoutComponent,
    ToolbarLayoutComponent,
    LogInComponent,
    MenuComponent,
    LoadingComponent,
    HomeMenuComponent,
    MenuUser2Component,
  ],
  exports:[ToolbarLayoutComponent,MenuUser2Component],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule,
    CoreModule,
    EditorModule,
    
    
  ], 
  providers: [
    
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
